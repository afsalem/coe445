function Calculate() {
	var c1 = parseInt(document.getElementById('cr1').value);
	var c2 = parseInt(document.getElementById('cr2').value);
	var totalCredits = c1 + c2;

	document.getElementById("totalCredits").innerHTML = totalCredits;

	// Compute GPA here 
	var grade1 = document.getElementById('g1').value; //letter grade
	var grade2 = document.getElementById('g2').value;

	var intger_grade1 = getIntegerGrade(grade1); //how much out of 4.0 is the grade?
	var intger_grade2 = getIntegerGrade(grade2);

	var pointsg1 = intger_grade1*c1; //how many points theses letter grade worth
	var pointsg2 = intger_grade2*c2;

	var gpa = (pointsg1+pointsg2)/totalCredits;
	document.getElementById("gpa").innerHTML = gpa;
};

function getIntegerGrade(x) { //converting grades into Quality Points
	var intger_GPA
	if (x == "A+"||x == "a+"){
		intger_GPA = 4.0;
	}
	else if(x == "A"||x == "a"){
		intger_GPA = 3.75;
	}
	else if(x == "B+"||x == "b+"){
		intger_GPA = 3.5;
	}
	else if(x == "B"||x == "b"){
		intger_GPA = 3.0;
	}
	else if(x == "C+"||x == "c+"){
		intger_GPA = 2.5;
	}
	else if(x == "C"||x == "c"){
		intger_GPA = 2.0;
	}
	else if(x == "D+"||x == "d+"){
		intger_GPA = 1.5;
	}
	else if(x == "D"||x == "d"){
		intger_GPA = 1.0;
	}
	else{
		intger_GPA = 0;
	}

	return intger_GPA;
}